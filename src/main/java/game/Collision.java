package game;

import game.Entity.Entity;

public class Collision {

    GamePanel gamePanel;

    public Collision(GamePanel gamePanel) {
        this.gamePanel = gamePanel;

    }

    public void checkCarre(Entity entity) {

        int entityLeftWorldX = entity.getX() + entity.getSolidArea().x;
        int entityRightWorldX = entity.getX() + entity.getSolidArea().x + entity.getSolidArea().width;
        int entityTopWorldY = entity.getY() + entity.getSolidArea().y;
        int entityBottomWorldY = entity.getY() + entity.getSolidArea().y + entity.getSolidArea().height;

        int entityLeftCol = entityLeftWorldX / gamePanel.CARRE_TAILLE;
        int entityRightCol = entityRightWorldX / gamePanel.CARRE_TAILLE;
        int entityTopRow = entityTopWorldY / gamePanel.CARRE_TAILLE;
        int entityBottomRow = entityBottomWorldY / gamePanel.CARRE_TAILLE;

        int carre1, carre2;

        switch (entity.getDirection()) {
            case UP -> {
                entityTopRow = (entityTopWorldY - entity.getSpeed()) / gamePanel.CARRE_TAILLE;
                carre1 = gamePanel.carreManager.getMap()[entityTopRow][entityLeftCol];
                carre2 = gamePanel.carreManager.getMap()[entityTopRow][entityRightCol];

                if (gamePanel.carreManager.getCarres()[carre1].collision || gamePanel.carreManager.getCarres()[carre2].collision) {
                    entity.setCollision(true);
                }
            }
            case DOWN -> {
                entityBottomRow = (entityBottomWorldY + entity.getSpeed()) / gamePanel.CARRE_TAILLE;
                carre1 = gamePanel.carreManager.getMap()[entityBottomRow][entityLeftCol];
                carre2 = gamePanel.carreManager.getMap()[entityBottomRow][entityRightCol];

                if (gamePanel.carreManager.getCarres()[carre1].collision || gamePanel.carreManager.getCarres()[carre2].collision) {
                    entity.setCollision(true);
                }
            }
            case LEFT -> {
                entityLeftCol = (entityLeftWorldX - entity.getSpeed()) / gamePanel.CARRE_TAILLE;
                carre1 = gamePanel.carreManager.getMap()[entityBottomRow][entityLeftCol];
                carre2 = gamePanel.carreManager.getMap()[entityTopRow][entityRightCol];

                if (gamePanel.carreManager.getCarres()[carre1].collision || gamePanel.carreManager.getCarres()[carre2].collision) {
                    entity.setCollision(true);
                }
            }
            case RIGHT -> {
                entityRightCol = (entityRightWorldX + entity.getSpeed()) / gamePanel.CARRE_TAILLE;
                carre1 = gamePanel.carreManager.getMap()[entityBottomRow][entityRightCol];
                carre2 = gamePanel.carreManager.getMap()[entityTopRow][entityRightCol];

                if (gamePanel.carreManager.getCarres()[carre1].collision || gamePanel.carreManager.getCarres()[carre2].collision) {
                    entity.setCollision(true);
                }
            }
        }


    }
}
