package game.carre;

import game.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class CarreManager {

    private GamePanel gamePanel;

    private Carre[] carres;

    private int[][] map;

    public CarreManager(GamePanel gamePanel) {

        this.gamePanel = gamePanel;

        this.carres = new Carre[70];

        this.map = new int[gamePanel.WORLD_MAX_ROW][gamePanel.WORLD_MAX_COL];

        getImages();
        loadMap();
    }

    public void getImages() {
        setupImage(0, "arbre1.png", true);
        setupImage(1, "arbre1.png", true);
        setupImage(2, "arbre1.png", true);
        setupImage(3, "arbre1.png", true);
        setupImage(4, "arbre1.png", true);
        setupImage(5, "arbre1.png", true);
        setupImage(6, "arbre1.png", true);
        setupImage(7, "arbre1.png", true);
        setupImage(8, "arbre1.png", true);
        setupImage(9, "arbre1.png", true);

        setupImage(10, "arbre1.png", true);
        setupImage(11, "chemin1.png", false);
        setupImage(12, "CT_B.png", false);
        setupImage(13, "CT_L.png", false);
        setupImage(14, "CT_LB.png", false);
        setupImage(15, "CT_LT.png", false);
        setupImage(16, "CT_R.png", false);
        setupImage(17, "CT_RB.png", false);
        setupImage(18, "CT_T.png", false);
        setupImage(19, "CT_TR.png", false);
        setupImage(20, "eau1.png", true);
        setupImage(21, "eau2.png", true);
        setupImage(22, "eauTerreBottom.png", true);
        setupImage(23, "eauTerreBottom2.png", true);
        setupImage(24, "eauTerreConnerLeftBottom.png", true);
        setupImage(25, "eauTerreConnerLeftTop.png", true);
        setupImage(26, "eauTerreConnerRightBottom.png", true);
        setupImage(27, "eauTerreConnerRightTop.png", true);
        setupImage(28, "eauTerreLeft.png", true);
        setupImage(29, "eauTerreRight.png", true);
        setupImage(30, "eauTerreTop.png", true);
        setupImage(31, "ET_B2.png", true);
        setupImage(32, "ET_L.png", true);
        setupImage(33, "ET_LB.png", true);
        setupImage(34, "ET_LT.png", true);
        setupImage(35, "ET_R.png", true);
        setupImage(36, "ET_RB.png", true);
        setupImage(37, "ET_TR.png", true);
        setupImage(38, "terre1.png", false);

    }

    private void setupImage(int indice, String fileName, boolean collision) {

        try {
            carres[indice] = new Carre();
            carres[indice].image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/terrain/" + fileName)));
            carres[indice].collision = collision;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void draw(Graphics2D d) {

        int[][] a = map;

        for (int ligne = 0; ligne < gamePanel.WORLD_MAX_ROW; ligne++) {

            for (int colonne = 0; colonne < gamePanel.WORLD_MAX_COL; colonne++) {

                int worldY = ligne * gamePanel.CARRE_TAILLE;
                int worldX = colonne * gamePanel.CARRE_TAILLE;

                int screenX = worldX - gamePanel.player.getX() + gamePanel.player.screenX;
                int screenY = worldY - gamePanel.player.getY() + gamePanel.player.screenY;

                if (worldX + gamePanel.CARRE_TAILLE > gamePanel.player.getX() - gamePanel.player.screenX
                        && worldX - gamePanel.CARRE_TAILLE < gamePanel.player.getX() + gamePanel.player.screenX
                        && worldY + gamePanel.CARRE_TAILLE > gamePanel.player.getY() - gamePanel.player.screenY
                        && worldY - gamePanel.CARRE_TAILLE < gamePanel.player.getY() + gamePanel.player.screenY
                ) {

                    d.drawImage(carres[map[ligne][colonne]].image, screenX, screenY, gamePanel.CARRE_TAILLE, gamePanel.CARRE_TAILLE, null);
                }
            }
        }
    }

    public void loadMap() {

        try {
            InputStream inputStream = getClass().getResourceAsStream("/map/map3.txt");


            if (inputStream == null) return;

            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

            String line;

            for (int ligne = 0; ligne < gamePanel.WORLD_MAX_ROW; ligne++) {

                if ((line = br.readLine()) != null) {

                    String[] nums = line.trim().split(" ");

                    for (int colonne = 0; colonne < nums.length; colonne++) {

                        map[ligne][colonne] = Integer.parseInt(nums[colonne]);

                    }

                } else break;

            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int[][] getMap() {
        return map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }

    public Carre[] getCarres() {
        return carres;
    }

    public void setCarres(Carre[] carres) {
        this.carres = carres;
    }
}
