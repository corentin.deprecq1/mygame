package game.Entity;

import game.GamePanel;
import game.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class Player extends Entity {

    GamePanel gamePanel;

    KeyHandler keyHandler;

    public final int screenX;
    public final int screenY;

    public Player(GamePanel gamePanel, KeyHandler keyHandler) {
        super();
        this.gamePanel = gamePanel;
        this.keyHandler = keyHandler;

        screenX = gamePanel.ECRAN_LARGEUR / 2 - (gamePanel.CARRE_TAILLE / 2);
        screenY = gamePanel.ECRAN_HAUTEUR / 2 - (gamePanel.CARRE_TAILLE / 2);

        this.setSolidArea(new Rectangle(8, 16, 32, 32));

        setDefaultValue();
        getImages();
    }

    private void setDefaultValue() {
        super.setSpeed(4);
        super.setDirection(Direction.RIGHT);
        super.setX(gamePanel.CARRE_TAILLE * 20);
        super.setY(gamePanel.CARRE_TAILLE * 34);
    }

    private void getImages() {

        try {
            this.setUp(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/up1.png"))));
            this.setUp2(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/up2.png"))));

            this.setDown(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/down1.png"))));
            this.setDown2(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/down2.png"))));

            this.setLeft(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/left1.png"))));
            this.setLeft2(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/left2.png"))));

            this.setRight(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/right1.png"))));
            this.setRight2(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/perso/right2.png"))));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void update() {

        if (keyHandler.right || keyHandler.down || keyHandler.left || keyHandler.up) {

            if (keyHandler.right) {
                this.setDirection(Direction.RIGHT);
            } else if (keyHandler.down) {
                this.setDirection(Direction.DOWN);
            } else if (keyHandler.left) {
                this.setDirection(Direction.LEFT);
            } else if (keyHandler.up) {
                this.setDirection(Direction.UP);
            }

            this.setCollision(false);
            gamePanel.getCollision().checkCarre(this);

            if (!this.isCollision()) {
                switch (this.getDirection()) {
                    case DOWN -> this.addY();
                    case UP -> this.removeY();
                    case LEFT -> this.removeX();
                    case RIGHT -> this.addX();
                }
            }

            addSprintConter();

            if (this.getSprintCounter() > 10) {

                if (this.getSpriteNum() == 1) {
                    this.setSpriteNum(2);
                } else if (this.getSpriteNum() == 2) {
                    this.setSpriteNum(1);
                }

                this.setSprintCounter(0);
            }
        }
    }

    public void draw(Graphics2D d) {

        BufferedImage image = null;

        switch (this.getDirection()) {
            case RIGHT -> {
                if (this.getSpriteNum() == 1) {
                    image = this.getRight();
                } else {
                    image = this.getRight2();
                }
            }
            case LEFT -> {
                if (this.getSpriteNum() == 1) {
                    image = this.getLeft();
                } else {
                    image = this.getLeft2();
                }

            }
            case UP -> {
                if (this.getSpriteNum() == 1) {
                    image = this.getUp();
                } else {
                    image = this.getUp2();
                }

            }
            case DOWN -> {
                if (this.getSpriteNum() == 1) {
                    image = this.getDown();
                } else {
                    image = this.getDown2();
                }
            }
        }

        d.drawImage(image, screenX, screenY, gamePanel.CARRE_TAILLE, gamePanel.CARRE_TAILLE, null);

    }
}
