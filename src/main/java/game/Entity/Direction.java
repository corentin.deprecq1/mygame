package game.Entity;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
