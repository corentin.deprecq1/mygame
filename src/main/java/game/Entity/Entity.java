package game.Entity;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Entity {

    private BufferedImage up, down, left, right;
    private BufferedImage up2, down2, left2, right2;

    private int speed;

    private int x;

    private int y;

    private Direction direction;

    private int sprintCounter = 0;

    private int spriteNum = 1;

    private Rectangle solidArea;

    private boolean collision = false;

    public Entity() {}



    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void addX() {
        this.x += this.speed;
    }

    public void removeX() {
        this.x -= this.speed;
    }

    public void addY() {
        this.y += this.speed;
    }

    public void removeY() {
        this.y -= this.speed;
    }

    public BufferedImage getUp() {
        return up;
    }

    public void setUp(BufferedImage up) {
        this.up = up;
    }

    public BufferedImage getDown() {
        return down;
    }

    public void setDown(BufferedImage down) {
        this.down = down;
    }

    public BufferedImage getLeft() {
        return left;
    }

    public void setLeft(BufferedImage left) {
        this.left = left;
    }

    public BufferedImage getRight() {
        return right;
    }

    public void setRight(BufferedImage right) {
        this.right = right;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public BufferedImage getUp2() {
        return up2;
    }

    public void setUp2(BufferedImage up2) {
        this.up2 = up2;
    }

    public BufferedImage getDown2() {
        return down2;
    }

    public void setDown2(BufferedImage down2) {
        this.down2 = down2;
    }

    public BufferedImage getLeft2() {
        return left2;
    }

    public void setLeft2(BufferedImage left2) {
        this.left2 = left2;
    }

    public BufferedImage getRight2() {
        return right2;
    }

    public void setRight2(BufferedImage right2) {
        this.right2 = right2;
    }

    public void addSprintConter() {
        this.sprintCounter++;
    }

    public int getSprintCounter() {
        return sprintCounter;
    }

    public void setSprintCounter(int sprintCounter) {
        this.sprintCounter = sprintCounter;
    }

    public int getSpriteNum() {
        return spriteNum;
    }

    public void setSpriteNum(int spriteNum) {
        this.spriteNum = spriteNum;
    }

    public Rectangle getSolidArea() {
        return solidArea;
    }

    public void setSolidArea(Rectangle solidArea) {
        this.solidArea = solidArea;
    }

    public boolean isCollision() {
        return collision;
    }

    public void setCollision(boolean collision) {
        this.collision = collision;
    }
}
