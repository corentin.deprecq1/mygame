package game;

import game.Entity.Player;
import game.carre.CarreManager;

import javax.swing.*;
import java.awt.*;

public class GamePanel extends JPanel implements Runnable {

    /* Screen Settings */
    public final int ORIGINAL_CARRE = 16;
    public final int SCALE = 3;
    public final int CARRE_TAILLE = ORIGINAL_CARRE * SCALE;
    public final int LIGNE_MAX = 20;
    public final int COLONNE_MAX = 20;
    public final int ECRAN_HAUTEUR = CARRE_TAILLE * LIGNE_MAX;
    public final int ECRAN_LARGEUR = CARRE_TAILLE * COLONNE_MAX;
    final int FPS = 60;

    /* World settings */
    public final int WORLD_MAX_ROW = 50;
    public final int WORLD_MAX_COL = 50;
    public final int WORLD_WIDTH = WORLD_MAX_COL * CARRE_TAILLE;
    public final int WORLD_HEIGHT = WORLD_MAX_ROW * CARRE_TAILLE;



    Thread gameThread;

    KeyHandler keyHandler = new KeyHandler();

    CarreManager carreManager = new CarreManager(this);

    public Player player = new Player(this, keyHandler);

    Collision collision = new Collision(this);

    public Collision getCollision() {
        return collision;
    }

    public void setCollision(Collision collision) {
        this.collision = collision;
    }

    public GamePanel() {
        this.setPreferredSize(new Dimension(ECRAN_LARGEUR, ECRAN_HAUTEUR));
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true);
        this.addKeyListener(keyHandler);
        this.setFocusable(true);
    }

    public void stratGameThread() {
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void run() {

        double drawInterval = (double) 1000000000 / FPS;
        double nextDraw = System.nanoTime() + drawInterval;

        while (gameThread != null) {

            /* mise à jour information */
            update();

            /* mise à jour écran */
            repaint();

            try {

                double remainingTime = nextDraw - System.nanoTime();

                remainingTime = remainingTime / 1000000;

                if (remainingTime < 0) {
                    remainingTime = 0;
                }

                Thread.sleep((long) remainingTime);

                nextDraw += drawInterval;


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void update() {

        player.update();

    }

    public void paintComponent(Graphics graphics) {

        super.paintComponent(graphics);

        Graphics2D graphics2D = (Graphics2D) graphics;

        carreManager.draw(graphics2D);
        player.draw(graphics2D);

        graphics2D.dispose();
    }
}
